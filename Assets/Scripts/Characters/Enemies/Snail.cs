﻿using UnityEngine;
using UnityEngine.UI;

public class Snail : Enemy
{
    public bool isInVehicle = false;

    private Weapon m_weapon;

    public GameObject[] heads;
    public bool randomizeHead = false;

    private bool isAttacking = false;

    private void Awake()
    {
        if (randomizeHead)
            heads[Random.Range(0, heads.Length - 1)].SetActive(true);
    }

    protected internal virtual void OnEnable()
    {
        if (_PlayerManager.Instance != null)
        {
            if (_PlayerManager.Instance.cam.Follow != null)
            {
                target = _PlayerManager.Instance.cam.Follow;
            }
        }
        m_weapon = GetComponentInChildren<Weapon>();
        m_weapon.projectile.GetComponent<ParticleSystem>().SetEmissionRate(0);
    }

    protected internal virtual void OnDestroy()
    {
        if (this.stats.health <= 0)
        {
            if (_EnemyManager.Instance)
            {
                _EnemyManager.Instance.SpawnEnemy();
            }
        }
    }

    private void EnableAttack()
    {
        if (!isAttacking)
        {
            isAttacking = true;
        }
    }

    private void DisableAttack()
    {
        if (isAttacking)
        {
            isAttacking = false;
        }
    }

    private void Attack()
    {
        if (isInVehicle)
        {
            isAttacking = true;
            target = this.transform;
        }

        if (isAttacking && target)
        {
            this.GetComponent<AudioSource>().enabled = true;

            m_weapon.FireProjectile();
        }
        else
        {
            if (m_weapon.projectile.GetComponent<ParticleSystem>().GetEmissionRate() == 0)
                return;

            this.GetComponent<AudioSource>().enabled = false;

            m_weapon.isFiring = false;
            m_weapon.projectile.GetComponent<ParticleSystem>().SetEmissionRate(0);
        }
    }


    private void ChaseTarget()
    {
        if (isInVehicle) return;
        if (target)
        {
            this.rigidbody.constraints = RigidbodyConstraints.None;
            Vector3 relativePos = target.position - transform.position;
            Quaternion rotation = Quaternion.LookRotation(relativePos);
            transform.rotation = rotation;

            attackTimer -= Time.deltaTime;
            if (attackTimer <= 0)
            {
                isAttacking = !isAttacking;
                attackTimer = Random.Range(1, 5);
            }

            Vector3 velocity = rigidbody.velocity;
            velocity = transform.forward * stats.speed * Time.deltaTime;
            rigidbody.velocity = velocity;

            //rigidbody.MovePosition(transform.position + offset + relativePos * stats.speed * Time.deltaTime);
            //anim.Play();
        }
    }

    new void Move()
    {
        if (isInVehicle) return;
        ChaseTarget();
    }

    new void FixedUpdate()
    {
        if (isInVehicle) return;
        Move();
    }

    private void Update()
    {
        Attack();
    }
}
