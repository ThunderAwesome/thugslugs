﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour {

    public void SceneIndex(int index)
    {
        SceneManager.LoadScene(index, LoadSceneMode.Single);
    }
}
