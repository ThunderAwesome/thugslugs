﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(BoxCollider))]
public class Player : Character
{
    public _PlayerManager.PlayerNumber PlayerNumber = _PlayerManager.PlayerNumber.One;

    private string saveData;

    public Slider healthbarSlider;
    private GameObject m_healthbar;

    private Weapon m_weapon;

    public GameObject playerPrefab;

    private PlayerActions Actions;

    void Start()
    {
        //Cursor.visible = false;

        rigidbody = GetComponent<Rigidbody>();
        InitHealthBar();
        //CameraFollow.Instance.Followed = this.rigidbody;
    }

    private void InitHealthBar()
    {
        if (!m_healthbar)
        {
            m_healthbar = Instantiate(healthbarSlider.gameObject);
            m_healthbar.transform.SetParent(GameObject.FindWithTag("UICanvas").transform, false);
        }
        m_healthbar.SetActive(true);
        m_healthbar.GetComponent<Slider>().maxValue = this.stats.maxHealth;
        m_healthbar.GetComponent<Slider>().minValue = 0;
    }

    public void HealthBar()
    {
        if (m_healthbar)
        {
            m_healthbar.GetComponent<Slider>().value = this.stats.health;
        }
    }

    protected internal virtual void OnEnable()
    {
        // See PlayerActions.cs for this setup.
        Actions = PlayerActions.CreateWithDefaultBindings();

        stats.health = stats.maxHealth;

        if (_PlayerManager.Instance != null)
        {
            _PlayerManager.Instance.cam.Follow = transform;
            _PlayerManager.Instance.cam.LookAt = transform;
        }

        m_weapon = GetComponentInChildren<Weapon>();
        m_weapon.projectile.GetComponent<ParticleSystem>().SetEmissionRate(0);

        LoadBindings();

        InitHealthBar();
    }


    protected internal virtual void OnDisable()
    {
        // This properly disposes of the action set and unsubscribes it from
        // update events so that it doesn't do additional processing unnecessarily.
        Actions.Destroy();
    }

    private void OnDestroy()
    {
        if (this.stats.health <= 0)
        {
            if (m_healthbar)
            {
                m_healthbar.SetActive(false);
            }

            if (_PlayerManager.Instance)
            {
                _PlayerManager.Instance.RevivePlayer(playerPrefab, _PlayerManager.Instance.spawnPoint.position, PlayerNumber);
            }
        }
    }

    protected internal virtual void LoadBindings()
    {
        if (PlayerPrefs.HasKey("Bindings"))
        {
            saveData = PlayerPrefs.GetString("Bindings");
            Actions.Load(saveData);
        }
    }

    private void Attack()
    {
        if (Actions.Attack.IsPressed)
        {
            this.GetComponent<AudioSource>().enabled = true;
            m_weapon.FireProjectile();
        }
        else
        {
            if (m_weapon.projectile.GetComponent<ParticleSystem>().GetEmissionRate() == 0)
                return;

            this.GetComponent<AudioSource>().enabled = false;

            m_weapon.isFiring = false;
            m_weapon.projectile.GetComponent<ParticleSystem>().SetEmissionRate(0);
        }
    }

    private void Move()
    {
        if (Actions.Move.IsPressed)
        {
            //rigidbody.MovePosition(transform.position + new Vector3(Actions.Move.X, transform.position.y, Actions.Move.Y) * stats.speed * Time.deltaTime);
            Vector3 velocity = rigidbody.velocity;

            velocity =  transform.forward * (Actions.Move.Y * stats.speed * Time.deltaTime);

            rigidbody.velocity = velocity;

            anim.Play();
        }
        else
        {
            anim.Pause();
        }
    }

    //private void LateUpdate()
    //{
    //    if (Actions.Rotate.IsPressed)
    //    {
    //        //set the rotation vector direction to face
    //        var moveDi = new Vector3(Actions.Rotate.X, 0, Actions.Rotate.Y);

    //        //find the wanted rotation angle based on the rotation vector
    //        Quaternion wanted_rotation = Quaternion.LookRotation(moveDi, Vector3.up);

    //        //set the player's rotation to rotate towards the last inputed rotation vector direction
    //        transform.rotation = Quaternion.RotateTowards(transform.rotation, wanted_rotation,
    //            stats.rotateSpeed * Time.deltaTime);
    //    }
    //    else
    //    {
    //        anim.Pause();
    //    }
    //}

    void FixedUpdate()
    {
        //		var heading = this.transform.position - (target.position);
        //		var distance = heading.magnitude;
        //		var _direction = heading / distance;
        Move();
    }

    private void Update()
    {
        HealthBar();
        Attack();
    }
}
