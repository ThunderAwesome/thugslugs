﻿using UnityEngine;
using System.Collections;

public class Caustic : MonoBehaviour
{

	private Light light;
	public float speed = .5f;
	public float offset = 8;
	public float maxValue = 5;
	private int i = 0;

	public Texture[] cookies;

	private void Start ()
	{
		light = this.GetComponent<Light> ();
		InvokeRepeating ("MoveLight", 0, speed);
	}

	private void MoveLight ()
	{
		//light.cookieSize = Mathf.PingPong (Time.time, maxValue) + offset;

		if (i < cookies.Length) {
			light.cookie = cookies [i];
			i++;
		} else
			i = 0;
	}
}
