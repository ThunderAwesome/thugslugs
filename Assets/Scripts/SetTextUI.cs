﻿using UnityEngine;
using UnityEngine.UI;

public class SetTextUI : MonoBehaviour
{

    private void OnEnable()
    {
        if (_PlayerManager.Instance != null)
        {
            _PlayerManager.Instance.playerLivesText = GetComponent<Text>();
        }
    }
}
