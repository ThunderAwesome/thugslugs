﻿using System;
using InControl;
using UnityEngine;

public class PlayerActions : PlayerActionSet
{
	public PlayerAction Left;
	public PlayerAction Right;
	public PlayerAction Up;
	public PlayerAction Down;

    public PlayerAction LeftStrafe;
    public PlayerAction RightStrafe;
    public PlayerAction UpStrafe;
    public PlayerAction DownStrafe;

    public PlayerTwoAxisAction Move;
    public PlayerTwoAxisAction Rotate;
	public PlayerAction Jump;
	public PlayerAction Attack;
	public PlayerAction Special;
	public PlayerAction Block;

	public PlayerAction Pause;
	public PlayerAction Restart;
	public PlayerAction Exit;

	public PlayerActions ()
	{
		Jump = CreatePlayerAction ("Jump");
		Attack = CreatePlayerAction ("Attack");
		Special = CreatePlayerAction ("Special");
		Block = CreatePlayerAction ("Block");

		Left = CreatePlayerAction ("Left");
		Right = CreatePlayerAction ("Right");
		Up = CreatePlayerAction ("Up");
		Down = CreatePlayerAction ("Down");

        LeftStrafe = CreatePlayerAction("LeftStrafe");
        RightStrafe = CreatePlayerAction("RightStrafe");
        UpStrafe = CreatePlayerAction("UpStrafe");
        DownStrafe = CreatePlayerAction("DownStrafe");

        Move = CreateTwoAxisPlayerAction (LeftStrafe, RightStrafe, DownStrafe, UpStrafe);
        Rotate = CreateTwoAxisPlayerAction(Left, Right, Down, Up);

		Pause = CreatePlayerAction ("Pause");
		Restart = CreatePlayerAction ("Restart");
		Exit = CreatePlayerAction ("Exit");
	}

	public static PlayerActions CreateWithDefaultBindings ()
	{

		var actions = new PlayerActions ();

		actions.Pause.AddDefaultBinding (InputControlType.Start);
		actions.Restart.AddDefaultBinding (InputControlType.Menu);
		actions.Exit.AddDefaultBinding (InputControlType.Back);

		actions.Pause.AddDefaultBinding (Key.P);
		actions.Restart.AddDefaultBinding (Key.R);
		actions.Exit.AddDefaultBinding (Key.Escape);


		actions.Jump.AddDefaultBinding (Key.Space);
		actions.Jump.AddDefaultBinding (Key.Z);
		actions.Attack.AddDefaultBinding (Key.LeftControl);
		actions.Attack.AddDefaultBinding (Key.LeftCommand);
		actions.Attack.AddDefaultBinding (Key.X);
		actions.Special.AddDefaultBinding (Key.RightControl);
		actions.Special.AddDefaultBinding (Key.RightCommand);
		actions.Special.AddDefaultBinding (Key.C);
		actions.Block.AddDefaultBinding (Key.RightShift);
		actions.Block.AddDefaultBinding (Key.LeftShift);
		actions.Block.AddDefaultBinding (Key.V);

		actions.Jump.AddDefaultBinding (Mouse.MiddleButton);
		actions.Attack.AddDefaultBinding (Mouse.LeftButton);
		actions.Special.AddDefaultBinding (Mouse.RightButton);

		actions.Jump.AddDefaultBinding (InputControlType.Action1);
		actions.Attack.AddDefaultBinding (InputControlType.RightBumper);
        actions.Attack.AddDefaultBinding(InputControlType.RightTrigger);
        actions.Attack.AddDefaultBinding(InputControlType.LeftBumper);
        actions.Attack.AddDefaultBinding(InputControlType.LeftTrigger);
        actions.Special.AddDefaultBinding (InputControlType.Action3);
		actions.Block.AddDefaultBinding (InputControlType.Action4);

        actions.UpStrafe.AddDefaultBinding(Key.W);
        actions.DownStrafe.AddDefaultBinding(Key.S);
        actions.LeftStrafe.AddDefaultBinding(Key.A);
        actions.RightStrafe.AddDefaultBinding(Key.D);

        actions.Up.AddDefaultBinding(Mouse.PositiveY);
        actions.Down.AddDefaultBinding(Mouse.NegativeY);
        actions.Left.AddDefaultBinding(Mouse.NegativeX);
        actions.Right.AddDefaultBinding(Mouse.PositiveX);

        actions.ListenOptions.IncludeUnknownControllers = true;
		actions.ListenOptions.MaxAllowedBindings = 4;
		//			actions.ListenOptions.MaxAllowedBindingsPerType = 1;
		//			actions.ListenOptions.UnsetDuplicateBindingsOnSet = true;
		//			actions.ListenOptions.IncludeMouseButtons = true;

		actions.ListenOptions.OnBindingFound = ( action, binding) => {
			if (binding == new KeyBindingSource (Key.Escape)) {
				action.StopListeningForBinding ();
				return false;
			}
			return true;
		};

		actions.ListenOptions.OnBindingAdded += ( action, binding) => {
			Debug.Log ("Binding added... " + binding.DeviceName + ": " + binding.Name);
		};

		actions.ListenOptions.OnBindingRejected += ( action, binding, reason) => {
			Debug.Log ("Binding rejected... " + reason);
		};

		actions.Up.AddDefaultBinding (InputControlType.LeftStickUp);
		actions.Down.AddDefaultBinding (InputControlType.LeftStickDown);
		actions.Left.AddDefaultBinding (InputControlType.LeftStickLeft);
		actions.Right.AddDefaultBinding (InputControlType.LeftStickRight);

		actions.Up.AddDefaultBinding (InputControlType.DPadUp);
		actions.Down.AddDefaultBinding (InputControlType.DPadDown);
		actions.Left.AddDefaultBinding (InputControlType.DPadLeft);
		actions.Right.AddDefaultBinding (InputControlType.DPadRight);

		return actions;

	}

}
