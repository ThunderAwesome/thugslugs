﻿using UnityEngine;
using System.Collections;

public class DisableOverTime : MonoBehaviour {

	public float timer = 2;

	private void Start(){
		Invoke ("Disable", timer);
	}

	private void Disable(){
		Destroy (this.gameObject);
	}
}
