﻿using UnityEngine;
using UnityEngine.UI;

public class _EnemyManager : MonoBehaviour {

    public static _EnemyManager Instance;

    public GameObject[] enemies;

    public Transform[] spawnPoints;

    private int m_KOCount = 0;

    public Text enemyKOCount;

    private void Awake()
    {
        Instance = this;
    }

    public void SpawnEnemy()
    {
        int random = Random.Range(0, enemies.Length);
        int randomSpawnPoint = Random.Range(0, spawnPoints.Length);
        var go = Instantiate(enemies[random], spawnPoints[randomSpawnPoint].position, Quaternion.identity);
        go.SetActive(true);
        m_KOCount++;

        enemyKOCount.text = m_KOCount.ToString();
    }
}
