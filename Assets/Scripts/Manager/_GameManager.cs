﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class _GameManager : MonoBehaviour
{
    public static _GameManager Instance;

    // Use this for initialization
    void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 30;
    }

    public void LoadScene(int scene)
    {
        StartCoroutine(LoadTime(scene));
    }

    private IEnumerator LoadTime(int scene)
    {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(scene);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }
}
